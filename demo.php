<?php
/**
 * Copyright (c) 2012,上海二三四五网络科技股份有限公司
 * 摘    要：demo.php
 * 作    者：zhangwj
 * 修改日期：15/4/14
 */
header("Content-type: text/html; charset=gbk");

define('IN_RCPHP', true);
/**
 * 定义项目所在路径
 */
define("APP_PATH", dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Demo' . DIRECTORY_SEPARATOR);
/**
 * 定义项目是否开启debug模式
 */
define('APP_DEBUG', true);
/**
 * 应用版本号
 */
define('APP_VERSION', 0.1);
/**
 * APP Nampespace
 */
define('APP_NAMESPACE', "Demo");

/**
 * 引入框架主文件
 */
require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'RcPHP' . DIRECTORY_SEPARATOR . 'RcPHP.php';
