<?php
/**
 * Request class file.
 *
 * @author         RcPHP Dev Team
 * @copyright      Copyright (c) 2013,RcPHP Dev Team
 * @license        Apache License 2.0 {@link http://www.apache.org/licenses/LICENSE-2.0}
 * @package        Core
 * @since          1.0
 */
namespace RCPHP;

defined('IN_RCPHP') or exit('Access denied');

class Request
{

	/**
	 * Get request.
	 *
	 * @param string $index
	 * @param bool   $xss
	 * @return array|string
	 */
	public static function get($index = '', $xss = true)
	{
		$index = trim($index);

		$value = '';

		if(!empty($index) && isset($_GET[$index]))
		{
			$value = $_GET[$index];

			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					$value[$k] = $xss === true ? remove_xss(dhtmlspecialchars($v)) : dhtmlspecialchars($v);
				}
			}
			else
			{
				$value = $xss === true ? remove_xss(dhtmlspecialchars($value)) : dhtmlspecialchars($value);
			}
		}

		return $value;
	}

	/**
	 * Post request.
	 *
	 * @param string $index
	 * @param bool   $xss
	 * @return array|string
	 */
	public static function post($index = '', $xss = true)
	{
		$index = trim($index);

		$value = '';

		if(!empty($index) && isset($_POST[$index]))
		{
			$value = $_POST[$index];

			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					$value[$k] = $xss === true ? remove_xss(dhtmlspecialchars($v)) : dhtmlspecialchars($v);
				}
			}
			else
			{
				$value = $xss === true ? remove_xss(dhtmlspecialchars($value)) : dhtmlspecialchars($value);
			}
		}

		return $value;
	}

	/**
	 * Get or post request.
	 *
	 * @param string $index
	 * @param string $xss
	 * @return bool|string|array
	 */
	public static function all($index, $xss = true)
	{
		if(isset($_POST[$index]))
		{
			$value = self::post($index, $xss);
		}
		else
		{
			$value = self::get($index, $xss);
		}

		return $value;
	}

	/**
	 * Header request.
	 *
	 * @param string $index
	 * @param bool   $xss
	 * @return array|string
	 */
	public static function header($index, $xss = true)
	{
		$index = trim($index);

		$value = '';

		if(!empty($index) && isset($_SERVER[$index]))
		{
			$value = $_SERVER[$index];

			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					$value[$k] = $xss === true ? remove_xss(dhtmlspecialchars($v)) : dhtmlspecialchars($v);
				}
			}
			else
			{
				$value = $xss === true ? remove_xss(dhtmlspecialchars($value)) : dhtmlspecialchars($value);
			}
		}

		return $value;
	}
}
