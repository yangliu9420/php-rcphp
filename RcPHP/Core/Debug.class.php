<?php
/**
 * Debug class file.
 *
 * @author         RcPHP Dev Team
 * @copyright      Copyright (c) 2013,RcPHP Dev Team
 * @license        Apache License 2.0 {@link http://www.apache.org/licenses/LICENSE-2.0}
 * @package        Core
 * @since          1.0
 * @filesource
 */
namespace RCPHP;

use RCPHP\Util\Check;
use RCPHP\Util\String;

defined('IN_RCPHP') or exit('Access denied');

class Debug
{

	/**
	 * The execution time.
	 *
	 * @var array
	 */
	public static $time = array();

	/**
	 * Use memory.
	 *
	 * @var array
	 */
	public static $memory = array();

	/**
	 * Include files.
	 *
	 * @var array
	 */
	public static $includeFile = array();

	/**
	 * Debug information.
	 *
	 * @var array
	 */
	public static $info = array();

	/**
	 * The SQL statement.
	 *
	 * @var array
	 */
	public static $sqls = array();

	/**
	 * System
	 *
	 * @var array
	 */
	public static $systems = array();

	/**
	 * Start data.
	 *
	 * @return void
	 */
	public static function start()
	{
		self::$time['start'] = microtime(true);
		self::$memory['start'] = memory_get_usage();
	}

	/**
	 * End data.
	 *
	 * @return void
	 */
	public static function stop()
	{
		self::$time['stop'] = microtime(true);
		self::$memory['stop'] = memory_get_usage();
	}

	/**
	 * Calculate the perform data.
	 *
	 * @return number
	 */
	public static function spent()
	{
		$time = round(self::$time['stop'] - self::$time['start'], 4);
		$memory = round(self::$memory['stop'] - self::$memory['start'], 4);

		return array(
			'time' => $time,
			'memory' => $memory
		);
	}

	/**
	 * Catch the exception.
	 *
	 * @param \Exception $e
	 */
	public static function appException(\Exception $e)
	{
		$error = array();

		$error['message'] = $e->getMessage();
		$error['file'] = $e->getFile();
		$error['line'] = $e->getLine();
		$error['trace'] = $e->getTraceAsString();

		\RCPHP\Log::error(serialize($error), array(), "Exception");

		Controller::halt($error);
	}

	/**
	 * 捕获系统致命错误
	 *
	 * @return void
	 */
	public static function fatalError()
	{
		if($e = error_get_last())
		{
			switch($e['type'])
			{
				case E_ERROR:
				case E_PARSE:
				case E_CORE_ERROR:
				case E_COMPILE_ERROR:
				case E_USER_ERROR:
					ob_end_clean();
					Controller::halt($e);
					break;
			}
		}
	}

	/**
	 * 自定义错误处理
	 *
	 * @author zhangwj<zhangwj@2345.com>
	 * @param int    $errno
	 * @param string $errstr
	 * @param string $errfile
	 * @param int    $errline
	 */
	public static function appError($errno, $errstr, $errfile, $errline)
	{
		switch($errno)
		{
			case E_ERROR:
			case E_PARSE:
			case E_CORE_ERROR:
			case E_COMPILE_ERROR:
			case E_USER_ERROR:
				ob_end_clean();
				$errorStr = "$errstr " . $errfile . " 第 $errline 行.";
				Log::error("[$errno] " . $errorStr, array(), "Error");
				Controller::halt($errorStr);
				break;
			default:
				$errorStr = $errstr . ' ' . $errfile . " line " . $errline;
				self::addMessage($errorStr, 3);
				break;
		}
	}

	/**
	 * Add the error message.
	 *
	 * @param string $message
	 * @param int    $type
	 */
	public static function addMessage($message, $type = 0)
	{
		//判断是否开启调试
		if(defined('APP_DEBUG') && APP_DEBUG == 1)
		{
			switch($type)
			{
				case 0:
					self::$info[] = $message;
					break;
				case 2:
					self::$sqls[] = $message;
					break;
				case 3:
					self::$systems[] = $message;
					break;
			}
		}
	}

	/**
	 * Output debugging information.
	 *
	 * @return void
	 */
	public static function output()
	{
		self::stop();
		$efficiency = self::spent();

		$tracePageTabs = array(
			'BASE' => '基本',
			'FILE' => '文件',
			'ERR' => '错误',
			'SQL' => 'SQL',
			'DEBUG' => '调试'
		);

		$files = get_included_files();
		$info = array();
		foreach($files as $key => $file)
		{
			$info[] = $file . ' ( ' . tosize(filesize($file)) . ' )';
		}
		$trace = array();
		$base = array(
			'请求信息' => date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']) . ' ' . $_SERVER['SERVER_PROTOCOL'] . ' ' . $_SERVER['REQUEST_METHOD'] . ' : ' . $_SERVER['PHP_SELF'],
			'运行时间' => $efficiency['time'],
			'吞吐率' => number_format(1 / $efficiency['time'], 2) . ' req/s',
			'内存开销' => tosize($efficiency['memory']),
			'文件加载' => count(get_included_files())
		);
		foreach($tracePageTabs as $name => $title)
		{
			switch(strtoupper($name))
			{
				case 'BASE':
					$trace[$title] = $base;
					break;
				case 'FILE':
					$trace[$title] = $info;
					break;
				case 'SQL':
					$trace[$title] = self::$sqls;
					break;
				case 'ERR':
					$trace[$title] = self::$systems;
					break;
				case 'DEBUG':
					$trace[$title] = self::$info;
					break;
			}
		}

		if(RcPHP::getMethod() != 'Ajax')
		{
			Check::isClient() ? var_dump(String::auto_charset($trace)) : include RCPHP_PATH . 'Tpl' . DS . 'trace.php';
		}
	}
}