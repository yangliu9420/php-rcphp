<?php
/**
 * Page class file.
 *
 * @author         RcPHP Dev Team
 * @copyright      Copyright (c) 2013,RcPHP Dev Team
 * @license        Apache License 2.0 {@link http://www.apache.org/licenses/LICENSE-2.0}
 * @package        Net
 * @since          1.0
 */
namespace RCPHP\Util;

use RCPHP\Controller;
use RCPHP\Net\Http;
use RCPHP\RcPHP;
use RCPHP\Request;

defined('IN_RCPHP') or exit('Access denied');

class Page
{

	/**
	 * 连接网址
	 *
	 * @var string
	 */
	public $url;

	/**
	 * 当前页
	 *
	 * @var int
	 */
	public $page;

	/**
	 * list总数
	 *
	 * @var int
	 */
	public $total;

	/**
	 * 分页总数
	 *
	 * @var int
	 */
	public $pages;

	/**
	 * 分页列表数量
	 *
	 * @var int
	 */
	public $num;

	/**
	 * list允许放页码数量
	 *
	 * @var integer
	 */
	public $circle = 10;

	/**
	 * list中的坐标
	 *
	 * @var integer
	 */
	public $center;

	/**
	 * 第一页
	 *
	 * @var string
	 */
	public $firstPage;

	/**
	 * 上一页
	 *
	 * @var string
	 */
	public $prePage;

	/**
	 * 下一页
	 *
	 * @var string
	 */
	public $nextPage;

	/**
	 * 最后一页
	 *
	 * @var string
	 */
	public $lastPage;

	/**
	 * 分页附属说明
	 *
	 * @var string
	 */
	public $note;

	/**
	 * 是否为ajax分页模式
	 *
	 * @var bool
	 */
	public $isAjax;

	/**
	 * ajax分页的动作名称
	 *
	 * @var string
	 */
	public $ajaxActionName;

	/**
	 * 分页css名
	 *
	 * @var string
	 */
	public $styleFile;


	/**
	 * 构造函数
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->center = 3;
		$this->num = 10;
		$this->circle = 10;
		$this->isAjax = false;

		//define pager style params
		$this->firstPage = '第一页';
		$this->prePage = '上一页';
		$this->nextPage = '下一页';
		$this->lastPage = '最末页';
	}

	/**
	 * 获取/设置总页数
	 *
	 * @param int $page
	 * @return float|int
	 */
	public function totalPage($page = null)
	{
		if(is_null($page))
		{
			if(empty($this->total))
			{
				return 1;
			}
			else
			{
				return ceil($this->total / $this->num);
			}
		}
		else
		{
			return (int)$page;
		}
	}

	/**
	 * 获取当前页数
	 *
	 * @return int
	 */
	public function getPageNum()
	{
		$page = empty($this->page) ? 1 : (int)$this->page;

		return ($page > $this->pages) ? (int)$this->pages : $page;
	}

	/**
	 * 返回$this->num=$num.
	 *
	 * @param int $num
	 * @return $this
	 */
	public function num($num = null)
	{
		if(is_null($num))
		{
			$num = 10;
		}

		$this->num = (int)$num;

		return $this;
	}

	/**
	 * 设置总记录
	 *
	 * @param int $total
	 * @return $this
	 */
	public function total($total = null)
	{
		$this->total = (!is_null($total)) ? (int)$total : 0;

		return $this;
	}

	/**
	 * 返回$this->url=$url.
	 *
	 * @param string $url
	 * @return $this
	 */
	public function url($url = null)
	{
		if(is_null($url))
		{
			$params = RcPHP::getParams();

			if(array_key_exists("page", $params))
			{
				unset($params['page']);
			}
			$url = Http::createUrl(RcPHP::getController() . URL_PATHINFO_DEPR . RcPHP::getAction(), $params);

			if(empty($params))
			{
				$url .= "?page=";
			}
			else
			{
				$url .= "&page=";
			}
			unset($params);
		}

		if(empty($url))
		{
			Controller::halt('The argument of method : url() invalid in pager class!');
		}

		$this->url = trim($url);

		return $this;
	}

	/**
	 * 设置当前页码
	 *
	 * @param int $page
	 * @return $this
	 */
	public function page($page = null)
	{
		if(is_null($page))
		{
			$page = (int)Request::get("page");
			$page = (empty($page)) ? 1 : $page;
		}

		if(empty($page))
		{
			Controller::halt('The argument of method : page() invalid in pager class!');
		}

		$this->page = $page;

		return $this;
	}

	/**
	 * 设置当前页显示偏移量
	 *
	 * @param int $num
	 * @return $this
	 */
	public function center($num = 0)
	{
		$this->center = (int)$num;

		return $this;
	}

	/**
	 * 设置页码偏移量
	 *
	 * @param int $num
	 * @return $this
	 */
	public function circle($num = 10)
	{
		$this->circle = (int)$num;

		return $this;
	}

	/**
	 * 处理第一页,上一页
	 *
	 * @return string
	 */
	private function getFirstPage()
	{

		if($this->page == 1 || $this->pages <= 1)
		{
			return '<li class="am-disabled"><a href="javascript:;">' . $this->firstPage . '</a></li><li class="am-disabled"><a href="javascript:;">' . $this->prePage . '</a></li>';
		}

		if($this->isAjax === true)
		{
			$string = '<li><a href="' . $this->url . '1" onclick="' . $this->ajaxActionName . '(\'' . $this->url . '1\'); return false;">' . $this->firstPage . '</a></li><li class="pagelist_ext"><a href="' . $this->url . ($this->page - 1) . '" onclick="' . $this->ajaxActionName . '(\'' . $this->url . ($this->page - 1) . '\'); return false;">' . $this->prePage . '</a></li>';
		}
		else
		{
			$string = '<li><a href="' . $this->url . '1" target="_self">' . $this->firstPage . '</a></li><li class="pagelist_ext"><a href="' . $this->url . ($this->page - 1) . '" target="_self">' . $this->prePage . '</a></li>';
		}

		return $string;
	}

	/**
	 * 处理下一页,最后一页
	 *
	 * @return string
	 */
	private function getLastPage()
	{

		if($this->page == $this->pages || $this->pages <= 1)
		{
			return '<li class="am-disabled"><a href="javascript:;">' . $this->nextPage . '</a></li><li class="am-disabled"><a href="javascript:;">' . $this->lastPage . '</a></li>';
		}

		if($this->isAjax === true)
		{
			$string = '<li><a href="' . $this->url . ($this->page + 1) . '" onclick="' . $this->ajaxActionName . '(\'' . $this->url . ($this->page + 1) . '\'); return false;">' . $this->nextPage . '</a></li><li class="pagelist_ext"><a href="' . $this->url . $this->totalPages . '" onclick="' . $this->ajaxActionName . '(\'' . $this->url . $this->totalPages . '\'); return false;">' . $this->lastPage . '</a></li>';
		}
		else
		{
			$string = '<li><a href="' . $this->url . ($this->page + 1) . '" target="_self">' . $this->nextPage . '</a></li><li class="pagelist_ext"><a href="' . $this->url . $this->pages . '" target="_self">' . $this->lastPage . '</a></li>';
		}

		return $string;
	}

	/**
	 * 处理list内容
	 *
	 * @return string
	 */
	private function getList()
	{

		if(empty($this->pages) || empty($this->page))
		{
			return false;
		}

		if($this->pages > $this->circle)
		{
			if($this->page + $this->circle >= $this->pages + $this->center)
			{
				$start = $this->pages - $this->circle + 1;
				$end = $this->pages;
			}
			else
			{
				$start = ($this->page > $this->center) ? $this->page - $this->center + 1 : 1;
				$end = ($this->page > $this->center) ? $this->page + $this->circle - $this->center : $this->circle;
			}
		}
		else
		{
			$start = 1;
			$end = $this->pages;
		}

		$pageListQueue = '';
		for($i = $start; $i <= $end; $i++)
		{
			$pageListQueue .= ($this->page == $i) ? '<li class="am-active"><a href="javascript:;">' . $i . '</a></li>' : (($this->isAjax === true) ? '<li><a href="' . $this->url . $i . '" onclick="' . $this->ajaxActionName . '(\'' . $this->url . $i . '\'); return false;">' . $i . '</a></li>' : '<li><a href="' . $this->url . $i . '" target="_self">' . $i . '</a></li>');
		}

		return $pageListQueue;
	}

	/**
	 * 开启ajax分页模式
	 *
	 * @param string $action
	 * @return $this
	 */
	public function ajax($action)
	{
		if(!empty($action))
		{
			$this->isAjax = true;
			$this->ajaxActionName = $action;
		}

		return $this;
	}

	/**
	 * 输出处理完毕的HTML
	 *
	 * @return string
	 */
	public function output()
	{

		//支持长的url.
		$this->url = trim(str_replace(array(
			"\n",
			"\r"
		), '', $this->url));

		//获取总页数.
		$this->pages = $this->totalPage();

		//获取当前页.
		$this->page = $this->getPageNum();

		return ($this->total <= $this->num) ? '' : '<div class="am-fr"><ul class="am-pagination">' . $this->getFirstPage() . $this->getList() . $this->getLastPage() . '</ul></div>';
	}
}
